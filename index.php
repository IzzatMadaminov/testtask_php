<?php

require_once "vendor/autoload.php";
use TestTask\Helpers\CurlRequest;

function searchByKeyword($key) {
    try
    {
        $params = array('url' => 'https://search.ipaustralia.gov.au/trademarks/search/doSearch',
            'host' => 'search.ipaustralia.gov.au',
            'header' => '',
            'method' => 'POST', // 'POST','HEAD'
            'referer' => 'https://search.ipaustralia.gov.au/trademarks/search/advanced',
            'cookie' => 'XSRF-TOKEN=ec7f2c30-51a7-4d4a-bafd-1e1ee4fed04f; SESSIONTOKEN=eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJlM2NiYTY3NC1hYTZlLTRhZDUtYmM5MC00NmFhNzIzZGMyNjAiLCJzdWIiOiI3MTVkYzllZi03YjkzLTRhN2YtYjAyZC04NDNiNGNmNDA1NjciLCJpYXQiOjE2MzgwMjgzNDMsImV4cCI6MTYzODA1NzE0M30.mHFLEvIM7OJKjgGkp5qMQ97TzskjNDPy5ZROG7bC2iY5s0wPvdwU7Oc7eSDUPv9l68_d5AMi2mk3k9HWsrp2HQ; AWSALB=5KRdaLjVfCozNClPul+0dxoxz/iYS+JQyOByKg+C4VV02viBJicP0W1W93UgTrrHQuoVAyL7elSEp56LFHNlWVUGOCKpYjHIcr9Pw1rGqcvgSS1JBBJDFNmiQsj0; AWSALBCORS=5KRdaLjVfCozNClPul+0dxoxz/iYS+JQyOByKg+C4VV02viBJicP0W1W93UgTrrHQuoVAyL7elSEp56LFHNlWVUGOCKpYjHIcr9Pw1rGqcvgSS1JBBJDFNmiQsj0; _p=0; ak_bmsc=720BA317DB93222C2C9C6C9C8669CA3E~000000000000000000000000000000~YAAQxDkrF0xI1V59AQAA1WIeYg1Sfc4k6im/8b1QbQCxfbLpr+2aZQto9HjvwWFzB/QQJ+Zj1eJ+u5bDxfOI8cTBdZdEBZDUxn1iuQfw4jwd0Okk5vcDmK0dJmKJtZZ1W6be2hDycSHJSNEtOkYoGODwF1IOeMUxi8hFC3mQv92vOQkzOjr14K1Tn1Y5SK+C+4xN+NsJSL/m2OczdY5v+/fc6wFO/UOIdRzz4Q1Rducq2d0FVIUjVWcQUGMfewkrXGS+On9cVRrrl+YfFInbOTbZYQibwaldnRTKiLaCIQtDs710A6i7pjGif7wz6hcZWhK/iB+6lH3xwVhLdgt/P4GUso2cC7BgOR++WrkVt9tRdSOIPhnseV1o50w+IddByUkjm415RQlLEK0XLfzWaW0=; bm_sv=0545C1F210BCE22F5009B0A2ACA56112~uTFBlmdIhmPb/JaOkw/isY3tM3DxLZImqQHjGj8whHQZkCbcxaLaJaiKPw61ZXrvmnKdqMhoyx2scM1aVsYw3V/1DV4dyLHp4YNxvqvQg0JLe/cykNAhbzB75T198Tae0iE1ZDdDOADIjPuMQ8MndwkKAqATtZglMki9dxY70Is=; bm_mi=5B5F239387EF83471D8689744FA6DA0F~MtClWg08DnEKfpNqMAJZwD+LDny2K+0DFfYuexCcMgWQRhu2xI0OpaQneYXPasAAvwa8b5g+7sUkDpMt/vDR4+TyQo3qp8RXiJ9AugxMhgJYP7sAFn3GA/3QrrweiqCWE6zJ8WEBkZGoBjIzOCdZYv84N/TjSJ4uUt2kAkGZBO0AC3RapM5S25a4tqFF/+EgymuhkC2va5QuSBlS8GmGljVkNZz75agXLhCaYShmtu68a9SHgiyP/LVfxSeVLEEV',
            'post_fields' => '_csrf=ec7f2c30-51a7-4d4a-bafd-1e1ee4fed04f&wt%5B0%5D=PART&weOp%5B0%5D=AND&wv%5B1%5D=&wt%5B1%5D=PART&wrOp=AND&wv%5B2%5D=&wt%5B2%5D=PART&weOp%5B1%5D=AND&wv%5B3%5D=&wt%5B3%5D=PART&iv%5B0%5D=&it%5B0%5D=PART&ieOp%5B0%5D=AND&iv%5B1%5D=&it%5B1%5D=PART&irOp=AND&iv%5B2%5D=&it%5B2%5D=PART&ieOp%5B1%5D=AND&iv%5B3%5D=&it%5B3%5D=PART&wp=&_sw=on&classList=&ct=A&status=&dateType=LODGEMENT_DATE&fromDate=&toDate=&ia=&gsd=&endo=&nameField%5B0%5D=OWNER&name%5B0%5D=&attorney=&oAcn=&idList=&ir=&publicationFromDate=&publicationToDate=&i=&c=&originalSegment=&wv%5B0%5D=' . urlencode($key), // 'var1=value&var2=value
            'timeout' => 180
        );

        $cur = new CurlRequest();
        $cur->init($params);
        $result = $cur->exec();
        if (!is_array($result)) {

            $url = $result;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_URL, "https://search.ipaustralia.gov.au/trademarks/search/count?" . $params['post_fields']);
            $count = curl_exec($ch);
            if (!json_decode($count, true)['errors']) {
                $count = json_decode($count, true)['count'];
            } else {
                $count = 0;
            }

                $data = [];
            for ($i = 0; $i <= ceil($count / 100); $i++) {
                curl_setopt($ch, CURLOPT_URL, $url . "&p=$i"); // for paginations
                $res = curl_exec($ch);

                $doc = new DOMDocument();
                @$doc->loadHTML($res);


                $table = $doc->getElementById("resultsTable");

                if ($table) {
                    $trs = $table->getElementsByTagName("tr");
                    foreach($trs as $tr) {
                        $temp =[];
                        if ($tr->hasAttribute("data-markurl")) {
                            foreach ($tr->getElementsByTagName("a") as $a)
                                $temp['number'] = $a->nodeValue;

                            foreach ($tr->getElementsByTagName("img") as $img)
                                $temp['logo_url'] = $img->getAttribute("src");

                            foreach ($tr->getElementsByTagName("td") as $td) {
                                if ($td->getAttribute("class") === "trademark words") {
                                    $temp['name'] = trim($td->nodeValue);
                                }
                                if ($td->getAttribute("class") === "classes ") {
                                    $temp['classes'] = trim($td->nodeValue);
                                }
                                if ($td->getAttribute("class") === "status") {
                                    if ($td->getElementsByTagName("div")[0]) {
                                        $status_array = explode(':', trim($td->getElementsByTagName("span")[0]->nodeValue));
                                        $temp['status1'] = $status_array[0];
                                        $temp['status2'] = array_key_exists(1, $status_array) ? $status_array[1] : '';
                                    } else {
                                        $td->removeCHild($td->getElementsByTagName("i")[0]);
                                        $status_array = explode(':', trim($td->nodeValue));
                                        $temp['status1'] = $status_array[0];
                                        $temp['status2'] = array_key_exists(1, $status_array) ? $status_array[1] : '';
                                    }
                                }
                            }

                            $temp['details_page_url'] = 'https://search.ipaustralia.gov.au' . $tr->getAttribute("data-markurl");
                            $data[] = $temp;
                        }
                    }
                }
            }

            print_r(json_encode(["results" => $data, "count" => $count]));
        }

    }
    catch (Exception $e)
    {
        echo $e->getMessage();
    }
}