<?php


namespace TestTask\Helpers;


/**
 * CLASS CurlRequest
 * @author Izzat Madaminov
 */
class CurlRequest
{
    private $ch;

    public function init($params)
    {
        $this->ch = curl_init();
        $user_agent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0';
        $header = array(
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
            "Accept-Language: en-US,en;q=0.5",
            "Accept-Encoding: gzip, deflate, br",
            "Content-Type: application/x-www-form-urlencoded",
            "Content-Length: " . strlen($params['post_fields']),
            "Origin: https://search.ipaustralia.gov.au",
            "Connection: keep-alive",
            "Upgrade-Insecure-Requests: 1",
            "Sec-Fetch-Dest: document",
            "Sec-Fetch-Mode: navigate",
            "Sec-Fetch-Site: same-origin",
            "Sec-Fetch-User: ?1");
        if (isset($params['host']) && $params['host'])
            $header[]="Host: ".$params['host'];
        if (isset($params['header']) && $params['header'])
            $header[]=$params['header'];

        @curl_setopt ( $this -> ch , CURLOPT_RETURNTRANSFER , 1 );
        @curl_setopt ( $this -> ch , CURLOPT_VERBOSE , 1 );
        @curl_setopt ( $this -> ch , CURLOPT_HEADER , 1 );

        @curl_setopt ( $this -> ch , CURLOPT_AUTOREFERER , 1 );
        @curl_setopt ( $this -> ch , CURLOPT_COOKIEFILE , false );

        if ($params['method'] == "HEAD")
            @curl_setopt($this -> ch,CURLOPT_NOBODY,1);

        @curl_setopt ( $this -> ch, CURLOPT_FOLLOWLOCATION, 0);
        @curl_setopt ( $this -> ch , CURLOPT_HTTPHEADER, $header );

        if ($params['referer'])
            @curl_setopt ($this -> ch , CURLOPT_REFERER, $params['referer'] );

        @curl_setopt ( $this -> ch , CURLOPT_USERAGENT, $user_agent);

        if ($params['cookie'])
            @curl_setopt ($this -> ch , CURLOPT_COOKIE, $params['cookie']);

        if ( $params['method'] == "POST" )
        {
            curl_setopt( $this -> ch, CURLOPT_POST, true );
            curl_setopt( $this -> ch, CURLOPT_POSTFIELDS, $params['post_fields'] );
        }

        @curl_setopt($this -> ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        @curl_setopt( $this -> ch, CURLOPT_URL, $params['url']);
        @curl_setopt ( $this -> ch , CURLOPT_SSL_VERIFYPEER, 0 );
        @curl_setopt ( $this -> ch , CURLOPT_SSL_VERIFYHOST, 0 );

        if (isset($params['login']) & isset($params['password']))
            @curl_setopt($this -> ch , CURLOPT_USERPWD,$params['login'].':'.$params['password']);
        @curl_setopt ( $this -> ch , CURLOPT_TIMEOUT, $params['timeout']);
    }

    /**
     * Make curl request
     *
     * @return array|bool|string
     */
    public function exec()
    {
        $response = curl_exec($this->ch);
        $target = curl_getinfo($this -> ch, CURLINFO_REDIRECT_URL);
        if ($target)
            return $target;
        $error = curl_error($this->ch);
        $result = array( 'header' => '',
            'body' => '',
            'curl_error' => '',
            'http_code' => '',
            'last_url' => '');
        if ( $error != "" )
        {
            $result['curl_error'] = $error;
            return $result;
        }

        $header_size = curl_getinfo($this->ch,CURLINFO_HEADER_SIZE);
        $result['header'] = substr($response, 0, $header_size);
        $result['body'] = substr( $response, $header_size );
        $result['http_code'] = curl_getinfo($this -> ch,CURLINFO_HTTP_CODE);
        $result['last_url'] = curl_getinfo($this -> ch,CURLINFO_EFFECTIVE_URL);
        return $response;
    }
}